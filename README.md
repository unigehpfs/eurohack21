# Palabos Summer School
# Monday
### (1) 9:00-10:30 Introductory session
Participants present themselves and their work, 5 minutes each.

### (2) 11:00-12:00 \[Practice\] Getting used to the environment
Introduction to the use of the containers. We make sure everyone can access the container and execute applications on their own hardware and/or on the provided GPU computers.

### (3) 13:30 - 14:00 Overview of Palabos GPU capabilities
We mostly show examples of what Palabos is capable of doing on GPUs, and on how GPUs are used in Palabos.

### (4) 14:00 - 15:00 \[Practice\] A first Palabos GPU application
The participants run the cavity and the porous-media examples, get the data to their computer and post-process with Paraview. Simple exercises are proposed to add features to the codes.

### (5) 15:15 - 15:45 Assessing GPU and multi-GPU performance
What performance do we expect to reach on GPU ? Explain the roofline performance model, the difference between consumer-level and cluster-level GPUs, the typical performance obtained on single- and multi-GPU executions, the bottlenecks.

### (6) 15:45 - 16:30 \[Practice\] Benchmarking a Palabos GPU application
We ask participants to take up again the examples executed before and look into the performance, with and without output, identify bottlenecks, etc.

# Tuesday
### (7) 9:00 - 10:00 Extending Palabos on GPU
This is a course on how to write annotate dynamics objects for GPU, and on how to write data processors for GPU.

### (8) 10:15 - 11:00 \[Practice\] Work with the Palabos multi-component code
We present and use the carbon-sequestration code. Simple questions are introduced to make sure the participants understand the different components of the code.  We make sure everyone is capable to retrieve the VTK files and visualize the data.

### (9) 11:00 - 12:00 \[Practice\] Implement pre- and post-processing capabilities
The participants write their own data processor to do data analytics.

### (10) 13:30 - 14:30 Keynote on multi-component flows in the lattice Boltzmann method
Xiaowen Shan's keynote.

### (11) 14:30 - 15:00 Presentation of the carbon sequestration problem
Challenges of the problem we are going to investigate. Repartition into groups.

### (12) 15:15 - 17:00 \[Practice\] Group work on carbon sequestration problem.
By the end of the afternoon, the groups should have submitted their jobs.

# Wednesday
### (13) 9:00 - 9:45  Background: an introduction to GPU programming using C++ standard parallelism

### (14) 9:45 - 10:30 \[Practice\] Writing a GPU application from scratch

### (15) 11:45 - 12:00 \[Practice\] Group work, evaluate results and prepare presentation

### (16) 13:30 - 15:00 Group Presentations


# TODO
## Attribution of sessions
| Session | Responsible | Task |
|--            |--                   |-- |
| 1 | Francesco |
| 2 | Pedro |
| 3 | *Christophe* | Overview Palabos on GPU |
| 4 | Jonas |
| 5 | *Pedro*  | Assessing GPU and multi-GPU performance |
| 6 | Karthik |
| 7 | *Jonas* | Extending Palabos on GPU |
| 8 | Francesco |
| 9 | Rémy |
| 10 | Xiaowen |
| 11 | Andrea |
| 12 | Pedro |
| 13 | *Rémy* | C++ standard parallelism |
| 14 | Karthik |
| 15 | Christophe |
| 16 | Jonas |


## Administrative tasks

| Task | Responsible |
|--    |--           |
| Call for added participants | Francesco |
| Merge request | Jonas |
| Dinner | Christophe |
| Name tags | Rémy |
| Containers | Pedro |
| GPU machines | Karthik |
| Accounts | Jonas |
