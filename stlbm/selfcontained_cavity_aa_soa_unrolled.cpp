// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// This program implements a lid-driven 3D cavity flow.
// It is a self-contained code extract of the stlbm library, which runs the BGK
// collision model with the AA-pattern structure-of-array scheme.
// The code is easy to adapt by copy-pasting other code portions from stlbm:
// - This code implements BGK (order-2), which you can replace by TRT by looking
//   up educational_lbm/aa_soa.h, or other collision models by looking up
//   the directory extended_collision_models/
// - This code uses "educational" implementations of BGK, i.e. without aggressive
//   loop unrolling. You can apply aggressive loop unrolling by looking up
//   optimized_lbm/aa_soa_bgk_unrolled.h
// - This code uses the AA-pattern and the structure-of-array
//   layout. To change this, replace the BGK class by another one found in
//   educational_lbm/, optimized_lbm/, or extended_collision_models/. If you move
//   away from the AA-pattern, you must also modify the "runCavity"
//   function: you're better off starting from one of the two other self-contained
//   codes.

#include <vector>
#include <array>
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <execution>
#include <chrono>
#include <cmath>

using namespace std;
using namespace std::chrono;

double Re        = 100.;  // Reynolds number
double ulb       = 0.02;  // Velocity in lattice units
int N            = 256;   // Number of nodes in x-direction
bool benchmark   = true;  // Run in benchmark mode ?
double max_t     = 10.0;  // Non-benchmark mode: Total time in dim.less units
int out_freq  = 200;      // Non-benchmark mode: Frequency in LU for output of terminal message and profiles (use 0 for no messages)
int data_freq = 0;        // Non-benchmark mode: Frequency in LU of full data dump (use 0 for no data dump)
int bench_ini_iter = 1000; // Benchmark mode: Number of warmup iterations
int bench_max_iter = 2000; // Benchmark mode: Total number of iterations

enum class CellType : uint8_t { bounce_back, bulk };

inline auto d3q19_constants() {
    // The discrete velocities of the d3q19 mesh.
    vector<array<int, 3>> c_vect = {
        {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
        {-1,-1, 0}, {-1, 1, 0}, {-1, 0,-1},
        {-1, 0, 1}, { 0,-1,-1}, { 0,-1, 1},
        { 0, 0, 0},
        { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
        { 1, 1, 0}, { 1,-1, 0}, { 1, 0, 1},
        { 1, 0,-1}, { 0, 1, 1}, { 0, 1,-1}
    };

    // The opposite of a given direction.
    vector<int> opp_vect =
        { 10, 11, 12, 13, 14, 15, 16, 17, 18, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8 };

    // The lattice weights.
    vector<double> t_vect =
        {
            1./18., 1./18., 1./18., 1./36., 1./36., 1./36., 1./36., 1./36., 1./36.,
            1./3.,
            1./18., 1./18., 1./18., 1./36., 1./36., 1./36., 1./36., 1./36., 1./36.
        };
    return make_tuple(c_vect, opp_vect, t_vect);
}

// Stores the number of elements in a rectangular-shaped simulation.
struct Dim {
    Dim(int nx_, int ny_, int nz_)
        : nx(nx_), ny(ny_), nz(nz_),
          nelem(static_cast<size_t>(nx) * static_cast<size_t>(ny) * static_cast<size_t>(nz)),
          npop(19 * nelem)
    { }
    int nx, ny, nz;
    size_t nelem, npop;
};

// Compute lattice-unit variables and discrete space and time step for a given lattice
// velocity (acoustic scaling).
auto lbParameters(double ulb, int lref, double Re) {
    double nu = ulb * static_cast<double>(lref) / Re;
    double omega = 1. / (3. * nu + 0.5);
    double dx = 1. / static_cast<double>(lref);
    double dt = dx * ulb;
    return make_tuple(nu, omega, dx, dt);
}

// Print the simulation parameters to the terminal.
void printParameters(bool benchmark, double Re, double omega, double ulb, int N, double max_t) {
    cout << "Lid-driven 3D cavity, " << (benchmark ? "benchmark" : "production") << " mode" << endl;
    cout << "N = " << N << endl;
    cout << "Re = " << Re << endl;
    cout << "omega = " << omega << endl;
    cout << "ulb = " << ulb << endl;
    if (benchmark) {
        cout << "Now running " << bench_ini_iter << " warm-up iterations." << endl;
    }
    else {
        cout << "max_t = " << max_t << endl;
    }
}

// Return a new clock for the current time, for benchmarking.
auto restartClock() {
    return make_pair(high_resolution_clock::now(), 0);
}

// Compute the time elapsed since a starting point, and the corresponding
// performance of the code in Mega Lattice site updates per second (MLups).
template<class TimePoint>
void printMlups(TimePoint start, int clock_iter, size_t nelem) {
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    double mlups = static_cast<double>(nelem * clock_iter) / duration.count();

    cout << "Benchmark result: " << setprecision(4) << mlups << " MLUPS" << endl;
}

// Fundamental data and methods for the Even and Odd classes.
struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;

    // Convert linear index to Cartesian indices.
    auto i_to_xyz (int i) {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    // Convert Cartesian indices to linear index.
    size_t xyz_to_i (int x, int y, int z) {
        return z + dim.nz * (y + dim.ny * x);
    };

    // Access the populations
    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    // Initialize the lattice with zero velocity and density 1.
    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            f(i, k) = t[k];
        }
    };

    // Compute the macroscopic variables density and velocity on a cell.
    auto macro (double& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6);
        double X_P1 = f(i, 10) + f(i, 13) + f(i, 14) + f(i, 15) + f(i, 16);
        double X_0  = f(i, 9) + f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i, 11) + f(i, 12) + f(i, 17) + f(i, 18);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i, 14);
        double Y_P1 = f(i, 4) + f(i, 11) + f(i, 13) + f(i, 17) + f(i, 18);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i, 16) + f(i, 18);
        double Z_P1 = f(i, 6) + f(i, 8) + f(i, 12) + f(i, 15) + f(i, 17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    // Compute the macroscopic variables from a temporary array.
    auto macropop (std::array<double, 19> const& pop) {
        double X_M1 = pop[0] + pop[3] + pop[4] + pop[5] + pop[6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[9] + pop[1] + pop[2] + pop[7] + pop[8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[1] + pop[3] + pop[7] + pop[8] + pop[14];
        double Y_P1 = pop[4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[2] + pop[5] + pop[7] + pop[16] + pop[18];
        double Z_P1 = pop[6] + pop[8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };
};

// Instances of this class are function objects: the function-call operator executes a
// collision-streaming cycle at even time steps.
struct Even : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    auto collideBgkUnrolled(int i, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u03 = u[0] + u[1];
        double ck_u04 = u[0] - u[1];
        double ck_u05 = u[0] + u[2];
        double ck_u06 = u[0] - u[2];
        double ck_u07 = u[1] + u[2];
        double ck_u08 = u[1] - u[2];

        double eq_00    = rho * t[ 0] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        double eq_01    = rho * t[ 1] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        double eq_02    = rho * t[ 2] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        double eq_03    = rho * t[ 3] * (1. - 3. * ck_u03 + 4.5 * ck_u03 * ck_u03 - usqr);
        double eq_04    = rho * t[ 4] * (1. - 3. * ck_u04 + 4.5 * ck_u04 * ck_u04 - usqr);
        double eq_05    = rho * t[ 5] * (1. - 3. * ck_u05 + 4.5 * ck_u05 * ck_u05 - usqr);
        double eq_06    = rho * t[ 6] * (1. - 3. * ck_u06 + 4.5 * ck_u06 * ck_u06 - usqr);
        double eq_07    = rho * t[ 7] * (1. - 3. * ck_u07 + 4.5 * ck_u07 * ck_u07 - usqr);
        double eq_08    = rho * t[ 8] * (1. - 3. * ck_u08 + 4.5 * ck_u08 * ck_u08 - usqr);
        double eq_09    = rho * t[ 9] * (1. - usqr);
        double eqopp_00 = eq_00 + rho * t[ 0] * 6. * u[0];
        double eqopp_01 = eq_01 + rho * t[ 1] * 6. * u[1];
        double eqopp_02 = eq_02 + rho * t[ 2] * 6. * u[2];
        double eqopp_03 = eq_03 + rho * t[ 3] * 6. * ck_u03;
        double eqopp_04 = eq_04 + rho * t[ 4] * 6. * ck_u04;
        double eqopp_05 = eq_05 + rho * t[ 5] * 6. * ck_u05;
        double eqopp_06 = eq_06 + rho * t[ 6] * 6. * ck_u06;
        double eqopp_07 = eq_07 + rho * t[ 7] * 6. * ck_u07;
        double eqopp_08 = eq_08 + rho * t[ 8] * 6. * ck_u08;

        double pop_out_00 = (1. - omega) * f(i,  0) + omega * eq_00;
        double pop_out_10 = (1. - omega) * f(i, 10) + omega * eqopp_00;
        f(i,  0) = pop_out_10;
        f(i, 10) = pop_out_00;

        double pop_out_01 = (1. - omega) * f(i,  1) + omega * eq_01;
        double pop_out_11 = (1. - omega) * f(i, 11) + omega * eqopp_01;
        f(i,  1) = pop_out_11;
        f(i, 11) = pop_out_01;

        double pop_out_02 = (1. - omega) * f(i,  2) + omega * eq_02;
        double pop_out_12 = (1. - omega) * f(i, 12) + omega * eqopp_02;
        f(i,  2) = pop_out_12;
        f(i, 12) = pop_out_02;

        double pop_out_03 = (1. - omega) * f(i,  3) + omega * eq_03;
        double pop_out_13 = (1. - omega) * f(i, 13) + omega * eqopp_03;
        f(i,  3) = pop_out_13;
        f(i, 13) = pop_out_03;

        double pop_out_04 = (1. - omega) * f(i,  4) + omega * eq_04;
        double pop_out_14 = (1. - omega) * f(i, 14) + omega * eqopp_04;
        f(i,  4) = pop_out_14;
        f(i, 14) = pop_out_04;

        double pop_out_05 = (1. - omega) * f(i,  5) + omega * eq_05;
        double pop_out_15 = (1. - omega) * f(i, 15) + omega * eqopp_05;
        f(i,  5) = pop_out_15;
        f(i, 15) = pop_out_05;

        double pop_out_06 = (1. - omega) * f(i,  6) + omega * eq_06;
        double pop_out_16 = (1. - omega) * f(i, 16) + omega * eqopp_06;
        f(i,  6) = pop_out_16;
        f(i, 16) = pop_out_06;

        double pop_out_07 = (1. - omega) * f(i,  7) + omega * eq_07;
        double pop_out_17 = (1. - omega) * f(i, 17) + omega * eqopp_07;
        f(i,  7) = pop_out_17;
        f(i, 17) = pop_out_07;

        double pop_out_08 = (1. - omega) * f(i,  8) + omega * eq_08;
        double pop_out_18 = (1. - omega) * f(i, 18) + omega * eqopp_08;
        f(i,  8) = pop_out_18;
        f(i, 18) = pop_out_08;

        f(i, 9) = (1. - omega) * f(i, 9) + omega * eq_09;
    }


    void iterateBgkUnrolled(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(f0);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
            collideBgkUnrolled(i, rho, u, usqr);
        }
    }

    // Execute an iteration (in this case, only a collision) on a cell.
    void operator() (double& f0) {
        iterateBgkUnrolled(f0);
    }
};

// Instances of this class are function objects: the function-call operator executes a
// collision-streaming cycle at odd time steps.
struct Odd : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    auto collideBgkUnrolled(std::array<double, 19>& pop, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u03 = u[0] + u[1];
        double ck_u04 = u[0] - u[1];
        double ck_u05 = u[0] + u[2];
        double ck_u06 = u[0] - u[2];
        double ck_u07 = u[1] + u[2];
        double ck_u08 = u[1] - u[2];

        double eq_00    = rho * t[ 0] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        double eq_01    = rho * t[ 1] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        double eq_02    = rho * t[ 2] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        double eq_03    = rho * t[ 3] * (1. - 3. * ck_u03 + 4.5 * ck_u03 * ck_u03 - usqr);
        double eq_04    = rho * t[ 4] * (1. - 3. * ck_u04 + 4.5 * ck_u04 * ck_u04 - usqr);
        double eq_05    = rho * t[ 5] * (1. - 3. * ck_u05 + 4.5 * ck_u05 * ck_u05 - usqr);
        double eq_06    = rho * t[ 6] * (1. - 3. * ck_u06 + 4.5 * ck_u06 * ck_u06 - usqr);
        double eq_07    = rho * t[ 7] * (1. - 3. * ck_u07 + 4.5 * ck_u07 * ck_u07 - usqr);
        double eq_08    = rho * t[ 8] * (1. - 3. * ck_u08 + 4.5 * ck_u08 * ck_u08 - usqr);
        double eq_09    = rho * t[ 9] * (1. - usqr);
        double eqopp_00 = eq_00 + rho * t[ 0] * 6. * u[0];
        double eqopp_01 = eq_01 + rho * t[ 1] * 6. * u[1];
        double eqopp_02 = eq_02 + rho * t[ 2] * 6. * u[2];
        double eqopp_03 = eq_03 + rho * t[ 3] * 6. * ck_u03;
        double eqopp_04 = eq_04 + rho * t[ 4] * 6. * ck_u04;
        double eqopp_05 = eq_05 + rho * t[ 5] * 6. * ck_u05;
        double eqopp_06 = eq_06 + rho * t[ 6] * 6. * ck_u06;
        double eqopp_07 = eq_07 + rho * t[ 7] * 6. * ck_u07;
        double eqopp_08 = eq_08 + rho * t[ 8] * 6. * ck_u08;

        pop[ 0] = (1. - omega) * pop[ 0] + omega * eq_00;
        pop[10] = (1. - omega) * pop[10] + omega * eqopp_00;

        pop[ 1] = (1. - omega) * pop[ 1] + omega * eq_01;
        pop[11] = (1. - omega) * pop[11] + omega * eqopp_01;

        pop[ 2] = (1. - omega) * pop[ 2] + omega * eq_02;
        pop[12] = (1. - omega) * pop[12] + omega * eqopp_02;

        pop[ 3] = (1. - omega) * pop[ 3] + omega * eq_03;
        pop[13] = (1. - omega) * pop[13] + omega * eqopp_03;

        pop[ 4] = (1. - omega) * pop[ 4] + omega * eq_04;
        pop[14] = (1. - omega) * pop[14] + omega * eqopp_04;

        pop[ 5] = (1. - omega) * pop[ 5] + omega * eq_05;
        pop[15] = (1. - omega) * pop[15] + omega * eqopp_05;

        pop[ 6] = (1. - omega) * pop[ 6] + omega * eq_06;
        pop[16] = (1. - omega) * pop[16] + omega * eqopp_06;

        pop[ 7] = (1. - omega) * pop[ 7] + omega * eq_07;
        pop[17] = (1. - omega) * pop[17] + omega * eqopp_07;

        pop[ 8] = (1. - omega) * pop[ 8] + omega * eq_08;
        pop[18] = (1. - omega) * pop[18] + omega * eqopp_08;

        pop[ 9] = (1. - omega) * pop[ 9] + omega * eq_09;
    }


    void iterateBgkUnrolled(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 19> pop;
            for (int k = 0; k < 19; ++k) {
                int XX = iX - c[k][0];
                int YY = iY - c[k][1];
                int ZZ = iZ - c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }

            auto[rho, u] = macropop(pop);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

            collideBgkUnrolled(pop, rho, u, usqr);

            for (int k = 0; k < 19; ++k) {
                int XX = iX + c[k][0];
                int YY = iY + c[k][1];
                int ZZ = iZ + c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
    }

    void operator() (double& f0) {
        iterateBgkUnrolled(f0);
    }
};

// Save the centerline velocity profiles into a text file to compare with reference values.
void saveProfiles(Even& lbm, double ulb)
{
    using namespace std;
    Dim const& dim = lbm.dim;
    ofstream middlex("middlex.dat");
    ofstream middley("middley.dat");
    int x1 = dim.nx / 2, x2 = dim.nx / 2,
        y1 = dim.ny / 2, y2 = dim.ny / 2,
        z1 = dim.nz / 2, z2 = dim.nz / 2;

    if (dim.nx % 2 == 0) x1--;
    if (dim.ny % 2 == 0) y1--;
    if (dim.nz % 2 == 0) z1--;

    for (int iX = 1; iX < dim.nx - 1; ++iX) {
        size_t i1 = lbm.xyz_to_i(iX, y1, z1);
        size_t i2 = lbm.xyz_to_i(iX, y2, z2);
        if (lbm.flag[i1] != CellType::bounce_back && lbm.flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.nx - 2);
            double pos = (static_cast<double>(iX) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lbm.lattice[i1]);
            auto [rho2, v2] = lbm.macro(lbm.lattice[i2]);
            middlex << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << 0.5 * (v1[0] + v2[0]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[1] + v2[1]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[2] + v2[2]) / ulb << "\n";
        }
    }

    for (int iY = 1; iY < dim.ny - 1; ++iY) {
        size_t i1 = lbm.xyz_to_i(x1, iY, z1);
        size_t i2 = lbm.xyz_to_i(x2, iY, z2);
        if (lbm.flag[i1] != CellType::bounce_back && lbm.flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.ny - 2);
            double pos = (static_cast<double>(iY) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lbm.lattice[i1]);
            auto [rho2, v2] = lbm.macro(lbm.lattice[i2]);
            middley << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << 0.5 * (v1[0] + v2[0]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[1] + v2[1]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[2] + v2[2]) / ulb << "\n";
        }
    }
}

// Save the velocity and density into a text file to produce images in post-processing.
void saveSlice(Even& lbm)
{
    Dim const& dim = lbm.dim;
    std::ofstream fvx("vx.dat");
    std::ofstream fvy("vy.dat");
    std::ofstream fvz("vz.dat");
    std::ofstream fv("v.dat");
    std::ofstream frho("rho.dat");
    for (int iX = dim.nx - 1; iX >= 0; --iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            size_t i = lbm.xyz_to_i(iX, iY, dim.nz / 2);
            auto [rho, v] = lbm.macro(lbm.lattice[i]);
            if (lbm.flag[i] == CellType::bounce_back) {
                rho = 1.0;
                v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                      lbm.f(i, 1) / (6. * lbm.t[1]),
                      lbm.f(i, 2) / (6. * lbm.t[2]) };
            }
            fvx << v[0] << " ";
            fvy << v[1] << " ";
            fvz << v[2] << " ";
            fv << std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]) << " ";
            frho << rho << " ";

        }
        fvx << "\n";
        fvy << "\n";
        fvz << "\n";
        fv << "\n";
        frho << "\n";
    }
}

// Compute the average kinetic energy in the domain.
double computeEnergy(Even& lbm)
{
    Dim const& dim = lbm.dim;
    double energy = 0.;
    for (int iX = 0; iX < dim.nx; ++iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iZ = 0; iZ < dim.nz; ++iZ) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                if (lbm.flag[i] != CellType::bounce_back) {
                    auto[rho, v] = lbm.macro(lbm.lattice[i]);
                    energy += v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
                }
            }
        }
    }
    energy *= 0.5;
    return energy;
}

// Initializes the flag matrix and the populations to set up a cavity flow. In particular, the
// momentum-exchange term is assigned to the wall cells.
void iniCavity(Even& lbm, double ulb, vector<double> const& ulid) {
    Dim const& dim = lbm.dim;
    for (size_t i = 0; i < dim.nelem; ++i) {
        auto[iX, iY, iZ] = lbm.i_to_xyz(i);
        if (iX == 0 || iX == dim.nx-1 || iY == 0 || iY == dim.ny-1 || iZ == 0 || iZ == dim.nz-1) {
            lbm.flag[i] = CellType::bounce_back;
            if (iX == dim.nx-1) {
                for (int k = 0; k < 19; ++k) {
                    lbm.f(i, k) = - 6. * lbm.t[k] * ulb * (
                        lbm.c[k][0] * ulid[0] + lbm.c[k][1] * ulid[1] + lbm.c[k][2] * ulid[2] );
                }
            }
            else {
                for (int k = 0; k < 19; ++k) {
                    lbm.f(i, k) = 0.;
                }
            }
        }
        else {
            lbm.flag[i] = CellType::bulk;
        }
    }
}

// Runs a simulation of a flow in a lid-driven cavity using the two-population scheme.
void runCavityAA(bool benchmark, double Re, double ulb, int N, double max_t)
{
    // CellData is either a double (structure-of-array) or an array<double, 19> (array-of-structure).
    using CellData = typename Even::CellData; 
    vector<double> ulid {0., 1., 0.}; // Velocity on top lid in dimensionless units.

    Dim dim {N, N, N};

    auto[nu, omega, dx, dt] = lbParameters(ulb, N - 2, Re);
    printParameters(benchmark, Re, omega, ulb, N, max_t);

    vector<CellData> lattice_vect(LBM::sizeOfLattice(dim.nelem));
    CellData* lattice = &lattice_vect[0];

    // The "vector" is used as a convenient way to allocate the flag array on the heap.
    vector<CellType> flag_vect(dim.nelem);
    CellType* flag = &flag_vect[0];

    // The lattice constants (discrete velocities, opposite indices, weghts) are mostly
    // used in the not-unrolled versions of the code. They must be allocated on the
    // heap so they can be shared with the device in case of a GPU execution.
    auto[c, opp, t] = d3q19_constants();
    // Instantiate two function objects for the for_each calls at even and at odd time
    // steps respectively. Note that collision and streaming are fused: only one of the
    // two is function objects is used at every time step.
    Even even{{lattice, flag, &c[0], &opp[0], &t[0], omega, dim}};
    Odd odd{{lattice, flag, &c[0], &opp[0], &t[0], omega, dim}};

    // Initialize the populations.
    for_each(lattice, lattice + dim.nelem, [&even](CellData& f0) { even.iniLattice(f0); });

    // Set up the geometry of the cavity.
    iniCavity(even, ulb, ulid);
    // Reset the clock, to be used when a benchmark simulation is executed.
    auto[start, clock_iter] = restartClock();

    // The average energy, dependent on time, can be used to monitor convergence, or statistical
    // convergence, of the simulation.
    ofstream energyfile("energy.dat");
    // Maximum number of time iterations depending on whether the simulation is in benchmark mode or production mode.
    int max_time_iter = benchmark ? bench_max_iter : static_cast<int>(max_t / dt);
    for (int time_iter = 0; time_iter < max_time_iter; ++time_iter) {
        if (benchmark && time_iter == bench_ini_iter)  {
            cout << "Now running " << bench_max_iter - bench_ini_iter
                 << " benchmark iterations." << endl;
            tie(start, clock_iter) = restartClock();
        }

        // If this option is chosen in the config file, save the raw populations in text files periodically.
        if (!benchmark && data_freq != 0 && time_iter % data_freq == 0 && time_iter > 0) {
            saveSlice(even);
        }
        // If this option is chosen in the config file, output some statistics and write the center-line
        // velocity profiles to a text file.
        if (!benchmark && out_freq != 0 && time_iter % out_freq == 0 && time_iter > 0) {
            cout << "Saving profiles at iteration " << time_iter
                 << ", t = " << setprecision(4) << time_iter * dt << setprecision(3)
                 << " [" << time_iter * dt / max_t * 100. << "%]" << endl;
            saveProfiles(even, ulb);
            double energy = computeEnergy(even) *dx*dx / (dt*dt);
            cout << "Average energy: " << setprecision(8) << energy << endl;
            energyfile << setw(10) << time_iter * dt << setw(16) << setprecision(8) << energy << endl;
        }

        // Collision and streaming are fused. Depending on the parity of the current time step,
        // execute one function object or the other one.
        if (time_iter % 2 == 0) {
            for_each(execution::par_unseq, lattice, lattice + dim.nelem, even);
        }
        else {
            for_each(execution::par_unseq, lattice, lattice + dim.nelem, odd);
        }
        ++clock_iter;
    }

    if (benchmark) {
        printMlups(start, clock_iter, dim.nelem);
    }
}

int main() {
    //tbb::task_scheduler_init init(1);
    runCavityAA(benchmark, Re, ulb, N, max_t);
    return 0;
}
